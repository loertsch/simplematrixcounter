#include <EEPROM.h>
#include <Bounce2.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include <Fonts/Lobster_Regular5pt7b.h>

#define EEPROM_READ(var) var = (EEPROM.read(0) << 8) + (EEPROM.read(1));
/*
// Disable this function for debugging, save your eeprom ;)
#define EEPROM_WRITE(var)              \
  EEPROM.update(0, (var >> 8) & 0xFF); \
  EEPROM.update(1, (var)&0xFF);
//*/
#define LED_PIN 8
#define BTN_PIN 4
#define PULLUP INPUT_PULLUP
#define DEBOUNCE_TIME 5
#define RETRIGGER_TIME 1000
//#define FONT Lobster_Regular5pt7b
#define MARGIN_TOP 1
#define FRAME_TIME (uint8_t)1000 / 30 //30fps
//#define EEPROM_WRITE // for debugging, leave enabled to save counter to eeprom

/**
 * How to reset the counter:
 * 1.) enable next line and flash on µC
 * 2.) when the µC starts, it should blink fast for 3 times
 * 3.) the counter is now reset, disable the next line again
 * 4.) flash the program once again, to return to normal operation mode
 */
// #define RESET

// MATRIX DECLARATION:
// Parameter 1 = width of NeoPixel matrix
// Parameter 2 = height of matrix
// Parameter 3 = pin number (most are valid)
// Parameter 4 = matrix layout flags, add together as needed:
//   NEO_MATRIX_TOP, NEO_MATRIX_BOTTOM, NEO_MATRIX_LEFT, NEO_MATRIX_RIGHT:
//     Position of the FIRST LED in the matrix; pick two, e.g.
//     NEO_MATRIX_TOP + NEO_MATRIX_LEFT for the top-left corner.
//   NEO_MATRIX_ROWS, NEO_MATRIX_COLUMNS: LEDs are arranged in horizontal
//     rows or in vertical columns, respectively; pick one or the other.
//   NEO_MATRIX_PROGRESSIVE, NEO_MATRIX_ZIGZAG: all rows/columns proceed
//     in the same order, or alternate lines reverse direction; pick one.
//   See example below for these values in action.
// Parameter 5 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, LED_PIN,
                                               NEO_MATRIX_TOP + NEO_MATRIX_LEFT +
                                                   NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
                                               NEO_GRB + NEO_KHZ800);

const uint16_t red = matrix.Color(255, 0, 0);

Bounce bounce = Bounce();

struct
{
  int x = 0;
  int y = MARGIN_TOP;
  int val_old = 0;
  int val_new = 0;
} digit[4];

uint16_t counter = 0;
unsigned long frameTimer = 0;
unsigned long retriggerTimer = 0;

void intToDigits(int n)
{
  int i = 3;
  do
  {
    digit[i].val_new = n % 10;
    n /= 10;
    i--;
  } while (n > 0);
}

void setup()
{

#ifdef RESET
#ifdef EEPROM_WRITE
  EEPROM_WRITE(0);
#endif
  for (int i = 0; i < 4; i++)
  {
    matrix.fillScreen(red); //Turn off all the LEDs
    matrix.show();
    delay(100);
    matrix.fillScreen(0); //Turn on all the LEDs
    matrix.show();
    delay(100);
  }
  exit(0);
#endif
  bounce.attach(BTN_PIN, INPUT_PULLUP);
  bounce.interval(DEBOUNCE_TIME); // interval in ms
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(5);
  matrix.setTextColor(red);
  matrix.setTextSize(1);
#ifdef FONT
  matrix.setFont(&FONT);
#endif
  digit[0].x = 2;
  digit[1].x = 9;
  digit[2].x = 17;
  digit[3].x = 25;
  EEPROM_READ(counter)
  intToDigits(counter);
}

void loop()
{
  bounce.update();
  if (bounce.changed() && millis() > retriggerTimer + RETRIGGER_TIME)
  {
    // THE STATE OF THE INPUT CHANGED
    int deboucedInput = bounce.read();
    // IF THE CHANGED VALUE IS LOW
    if (deboucedInput == HIGH)
    {
      counter++;
#ifdef EEPROM_WRITE
      EEPROM_WRITE(counter);
#endif
      intToDigits(counter);
      retriggerTimer = millis();
    }
  }

  if (millis() > frameTimer + FRAME_TIME)
  {
    matrix.fillScreen(0); //Turn off all the LEDs
    for (int i = 0; i < 4; i++)
    {
      matrix.setCursor(digit[i].x, digit[i].y);
      matrix.print(digit[i].val_old);
      if (digit[i].val_new != digit[i].val_old)
      {
        matrix.setCursor(digit[i].x, digit[i].y + matrix.height() + 2);
        matrix.print(digit[i].val_new);
        if (digit[i].y > -matrix.height() + MARGIN_TOP)
          digit[i].y--;
        else
        {
          digit[i].y = MARGIN_TOP;
          digit[i].val_old = digit[i].val_new;
        }
      }
    }
    matrix.show();
  }
}
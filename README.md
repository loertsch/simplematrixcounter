==== Dependencies ====

cd lib

git clone https://github.com/adafruit/Adafruit_NeoMatrix

git clone https://github.com/adafruit/Adafruit_NeoPixel

git clone https://github.com/adafruit/Adafruit-GFX-Library

git clone https://github.com/thomasfredericks/Bounce2



==== Einstellungen: ====

=== LED_PIN ===

Pin, auf dem Pixeldaten ausgegeben werden.

=== BTN_PIN ===

Pin, an dem Taster angeschlossen ist

=== PULLUP ===

Entweder INPUT oder INPUT_PULLUP

=== DEBOUNCE_TIME ===

Wie lange muss der Taster mindestens gedrückt werden um eine Aktion auszulösen in ms.

=== RETRIGGER_TIME ===

Wie lange wird gewartet, bis ein neuer Tastendruck registriert wird.

=== FONT ===

Bsp.: 
	#define FONT Lobster_Regular5pt7b
	#include <Fonts/Lobster_Regular5pt7b.h>
Exakter Name der Font, sie wird vom fontconverter immer so erzeugt, dass sie auf <Schriftname><Schriftgröße>pt7b endet.
Das include oben drüber muss dazu passen (#include <Fonts/FONT.h>)
Auskommentieren, um default-Schirftart zu nehmen.

=== MARGIN_TOP ===

Die Adafruit lib versucht, die Baseline bei allen Schriften gleich zu lassen und ermittelt daher einen automatischen Höhenoffset. Das verschiebt bei jeder Schriftart die Höhe
Mit dieser Option kann man die Schrift genau passend im Bild zentrieren. Muss für jede Schriftart neu getestet werden!

=== Adafruit_NeoMatrix ===

Normale Einstellung für 32x8 ZickZackPanel:
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, PIN,
						NEO_MATRIX_TOP + NEO_MATRIX_LEFT +
						NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
						NEO_GRB + NEO_KHZ800);

Einstellung um 180° gedreht (auf dem Kopf), wenn hier nicht das erste Pixel auf NEO_MATRIX_RIGHT geändert wird, erscheint Bild gespiegelt
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, PIN,
											   NEO_MATRIX_BOTTOM + NEO_MATRIX_RIGHT +
											   NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
											   NEO_GRB + NEO_KHZ800);


=== color ===

const uint16_t red = matrix.Color(255,0,0); 
Hier können beliebige RGB-Werte eingestellt werden.

=== setup() ===

Hier finden sich viele Einstellungen, kurzer überblick:
* X-Positionen der einzelnen Ziffern, kann angepasst werden, wenn der Zeichenabstand bei einer Font komisch aussieht, Werte in Pixeln von links
  digit[0].x = 2;
  digit[1].x = 9;
  digit[2].x = 17;
  digit[3].x = 25;
* matrix.setTextWrap(false); // immer aus lassen
* matrix.setBrightness(5); // Helligkeit
* matrix.setTextColor(red); // Farbe (s.O.)
* matrix.setTextSize(1); // Textgröße kann nur in ganzen Vielfachen angegeben werden, also z.B. 1 = 6x6pt, 2 = 12x12pt usw.

==== getestetes Verhalten: ====

* Bei Überlauf wird wieder bei 0 begonnen
* Nach ca. 50 Tagen kann bei den Timern ein Überlauf auftreten
* Der EEPROM hat 100000 Löschzyklen angegeben. Danach können die Zellen evtl. nicht mehr gelesen werden und Fehler auftreten. Dann sollte die EEPROM Adresse (im Moment 0 und 1) verschoben werden oder der Chip getauscht.

==== Eigene Schriftarten konvertieren: ====

* Benötigte libraries installieren:
	* sudo apt-get install libfreetype6-dev
* in Verzeichnes lib/Adafruit-GFX-Library/fontconvert wechseln
	* cd lib/Adafruit-GFX-Library/fontconvert
	* make
	* chmod +x fontconvert
* Konvertierung
	* ./fontconvert ../../../fonts/Lobster/Lobster-Regular.ttf 6 > ../Fonts/Lobster_Regular6pt7b.h
	* Wichtig: Name der Ausgabedatei muss immer mit XXpt7b.h enden, sonst findet er sie nicht

